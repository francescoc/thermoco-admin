# ThermoCo Admin Console
Sample app to manage ThermoCo sensors


## Start the project in development mode
In order the App in development scenario you should run the following commands:

### 1. `docker run -p 8000:8000 -it vintratest/thermo_api:latest`
To locally start the API server.

### 2. `yarn install`
To install the client app dependencies.

### 3. `yarn start`
To start the client app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


## Build the Client App

### `yarn build`

Builds the app for production to the `build` folder.


## Development Notes
Due to the time limitation it was impossible to:

  1. create a useful Data-Layer abstraction. It would be good in order to manage API requests, data caching, etc.
  2. implement form validation
  3. better manage 'loading' and 'error' states
  4. maintain the user session between tabs / refresh
  5. create unit testing
  6. improve look & feel and usability
