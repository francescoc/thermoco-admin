import React, {useState, useEffect} from 'react'
import { useParams, useHistory } from 'react-router'
import axios from 'axios'
import CircularProgress from '@material-ui/core/CircularProgress'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import InputAdornment from '@material-ui/core/InputAdornment'
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Button from '@material-ui/core/Button';

import ButtonLink from '../components/ButtonLink';


const SensorEditView = () => {
  const {id} = useParams()
  const routerHistory = useHistory()
  const isNew = id === 'new'
  const [loading, setLoading] = useState(false)
  const [sensorData, setSensorData] = useState(null)
  const [sensorIsActive, setSensorIsActive] = useState(true)

  const handleIsActiveChange = (e) => {
    setSensorIsActive(e.target.checked)
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    const formData = new FormData(e.target)
    const requestData = {
      description: formData.get('description'),
      samplingPeriod: formData.get('samplingPeriod'),
      isActive: sensorIsActive,
    }
    // TODO: add form validation
    if(isNew) {
      axios
      .post(`/api/v1/sensors`, requestData)
      .then(() => {
        routerHistory.push('/')
      })
    } else {
      axios
        .put(`/api/v1/sensors/${id}`, requestData)
        .then(() => {
          routerHistory.push('/')
        })
        // TODO: add error handling
    }
  }

  const handleDeleteClick = () => {
    var dialogResponse = window.confirm('Do you really want to delete this sensor?');
    if(dialogResponse) {
      axios
        .delete(`/api/v1/sensors/${id}`)
        .then(() => {
          routerHistory.push('/')
        })
    }
  }

  useEffect(() => {
    if(!isNew) {
      setLoading(true)
      axios
        .get(`/api/v1/sensors/${id}`)
        .then((response) => {
          const {data} = response
          setSensorData(data)
          setSensorIsActive(data.isActive)
          setLoading(false)
        })
    }
  }, [id, isNew])

  return (
    loading ? (
      <CircularProgress />
    ) : (
      <Card>
        <CardContent>
          <form onSubmit={handleSubmit}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <Typography component="h1" variant="h5">
                  {isNew ? 'Create Sensor' : `Edit Sensor`}
                </Typography>
              </Grid>

              <Grid item xs={12}>
                <TextField
                  name="description"
                  label="Description"
                  defaultValue={isNew ? 'New Sensor' : (sensorData && sensorData.description)}
                  variant="outlined"
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <FormControlLabel
                  control={
                    <Switch
                      checked={sensorIsActive}
                      onChange={handleIsActiveChange}
                      name="isActive"
                      color="primary"
                    />
                  }
                  label="Active"
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  name="samplingPeriod"
                  label="Sampling period"
                  defaultValue={isNew ? 'New Sensor' : (sensorData && sensorData.samplingPeriod)}
                  type="number"
                  InputProps={{
                    inputProps: {
                      min: 5
                    },
                    endAdornment: <InputAdornment position="end">Seconds</InputAdornment>
                  }}
                  variant="outlined"
                  fullWidth
                />
              </Grid>
              <Grid item xs={4}>
                <ButtonLink
                  to="/"
                  fullWidth
                  variant="outlined"
                >
                  Cancel
                </ButtonLink>
              </Grid>
              <Grid item xs={4}>
                {!isNew && (
                  <Button
                    onClick={handleDeleteClick}
                    fullWidth
                    variant="outlined"
                  >
                    Delete Sensor
                  </Button>
                )}
              </Grid>
              <Grid item xs={4}>
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                >
                  {isNew ? 'Create Sensor' :  'Update Sensor'}
                </Button>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
    )
  )
}

export default SensorEditView