import React, {useState, useEffect} from 'react'
import axios from 'axios'
import { makeStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Box from '@material-ui/core/Box'
import CircularProgress from '@material-ui/core/CircularProgress'
import AddIcon from '@material-ui/icons/Add'

import Link from '../components/Link'
import ButtonLink from '../components/ButtonLink'

const useStyles = makeStyles({
  table: {
    minWidth: 450,
  },
})

const SensorsListView = () => {
  const classes = useStyles()
  const [loading, setLoading] = useState(false)
  // const [error, setError] = useState(null)
  const [sensorsData, setSensorsData] = useState([])
  useEffect(() => {
    setLoading(true)
    axios
      .get('/api/v1/sensors/')
      .then((response) => {
        setSensorsData(response.data)
        setLoading(false)
      })
      // TODO: implement error logic
  }, [])

  return (
    loading ? (
      <CircularProgress />
    ) : (
      <Card>
        <CardContent>
          <TableContainer>
            <Table className={classes.table} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Description</TableCell>
                  <TableCell align="right">Status</TableCell>
                  <TableCell align="right">Sampling</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {sensorsData.map((row) => (
                  <TableRow key={row.id}>
                    <TableCell component="th" scope="row">
                      <Link to={`/${row.id}`}>
                        {row.description || '[No description]'}
                      </Link>
                    </TableCell>
                    <TableCell align="right">{row.isActive ? 'Active' : 'Inactive'}</TableCell>
                    <TableCell align="right">{row.samplingPeriod} seconds</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          <Box display="flex" justifyContent="flex-end" mt={3}>
            <ButtonLink
              variant="contained"
              color="primary"
              to="/new"
            >
              <AddIcon /> &nbsp;
              Add Sensor
            </ButtonLink>

          </Box>
        </CardContent>
      </Card>
    )
  )
}

export default SensorsListView