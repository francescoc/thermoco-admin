import React from 'react'
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Collapse from '@material-ui/core/Collapse'
import Alert from '@material-ui/lab/Alert'

import { useAuthStore } from '../AuthStore'


const LoginView = (props) => {
  const authStore = useAuthStore()

  const handleSubmit = (e) => {
    e.preventDefault()
    const formData = new FormData(e.target)
    authStore.login(formData.get('username'), formData.get('password'))
  }

  return (
    <Grid container justify="center">
      <Grid item md={4}>
        <Card>
          <CardContent>

            <Typography component="h1" variant="h5">
              Sign in
            </Typography>

            <form noValidate onSubmit={handleSubmit}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="username"
                label="Username"
                name="username"
                autoComplete="username"
                autoFocus
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="password"
                label="Password"
                name="password"
                type="password"
                autoComplete="current-password"
              />

              <Collapse in={!!authStore.error}>
                <Box mt={1} mb={2}>
                  <Alert severity="error">{authStore.error && authStore.error.detail}</Alert>
                </Box>
              </Collapse>

              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
              >
                Sign In
              </Button>
            </form>

          </CardContent>
        </Card>
      </Grid>
    </Grid>
  )
}

export default LoginView
