import {createContext, useContext, useState, useMemo} from 'react'
import axios from 'axios'

export const AuthStoreContext = createContext()

export const useAuthStore = () => useContext(AuthStoreContext)

export const AuthStoreProvider = (props = {}) => {
  const [error, setAuthError] = useState(null)
  const [logged, setLogged] = useState(false) //TODO: it should be
  const authStore = useMemo(() => {
    return {
      logged,
      error,
      login: (username, password) => {
        setAuthError(null)

        const params = new URLSearchParams()
        params.append('username', username)
        params.append('password', password)

        return axios
          .post('/auth/login', params, {
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            }
          })
          .then(function (response) {
            const {access_token} = response.data
            setLogged(true)
            axios.defaults.headers.Authorization = `Bearer ${access_token}`
          })
          .catch(function (error) {
            if(error && error.response) {
              setAuthError(error.response.data)
            }
          })
      },
      logout: () => {
        setLogged(false)
        delete axios.defaults.headers.Authorization
      }
    }
  }, [logged, error, setLogged])

  window.authStore = authStore

  return (
    <AuthStoreContext.Provider
      {...props}
      value={authStore}
    />
  )
}
