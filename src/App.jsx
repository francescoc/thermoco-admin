import React from 'react'
import axios from 'axios'
import CssBaseline from '@material-ui/core/CssBaseline'
import Container from '@material-ui/core/Container'
import Box from '@material-ui/core/Box'

import {API_ENDPOINT} from './constants'
import {AuthStoreProvider} from './AuthStore'
import AppRoutes from './AppRoutes'
import AppBar from './AppBar'

// configure api client basics
window.axios = axios
axios.defaults.baseURL = API_ENDPOINT

function App() {
  return (
    <AuthStoreProvider>
      <CssBaseline />
      <AppBar />
      <Container>
        <Box mt={4}>
          <AppRoutes />
        </Box>
      </Container>
    </AuthStoreProvider>
  )
}

export default App
