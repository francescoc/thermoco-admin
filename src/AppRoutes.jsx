import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"

import {useAuthStore} from './AuthStore'
import LoginView from './views/LoginView'
import SensorsListView from './views/SensorsListView'
import SensorEditView from './views/SensorEditView'

const AppRoutes = () => {
  const authStore = useAuthStore()

  return (
    <Router>
      <Switch>
        {!authStore.logged ? (
          <Route path="/">
            <LoginView />
          </Route>
        ) : (
          <>
            <Route path="/" exact>
              <SensorsListView />
            </Route>
            <Route path="/:id">
              <SensorEditView />
            </Route>
          </>
        )}
      </Switch>
    </Router>
  )
}

export default AppRoutes