import React from 'react'
import MuiButton from '@material-ui/core/Button'
import { Link as RouterLink } from "react-router-dom"

const ButtonLink = (props) => (
  <MuiButton component={RouterLink} {...props} />
)

export default ButtonLink
