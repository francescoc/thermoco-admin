import React from 'react'
import MuiLink from '@material-ui/core/Link'
import { Link as RouterLink } from "react-router-dom"

const Link = (props) => (
  <MuiLink component={RouterLink} {...props} />
)

export default Link
